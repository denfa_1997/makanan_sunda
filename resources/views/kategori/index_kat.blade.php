@extends('layouts.master')


@section('content') 
<div id="content">

                <!-- Topbar -->
                

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Tabel Pegawai</h1>
                   

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Tabel</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr class="text-center">
                                            <th style="width: 10px">No</th>
                                            <th>Rasa</th>
                                            <th style="width: 200px">Aksi</th>
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Tiger Nixon</td>
                                            <td>
                                            <div class="row">
                                                <div class="col-md-6 ">
                                                <a href="#" class="btn btn-warning btn-icon-split">
                                                    <span class="text">Edit</span>
                                                </a>
                                                </div>
                                                <div class="col-md-6">
                                                <a href="#" class="btn btn-danger btn-icon-split">
                                                <span class="text">Hapus</span>
                                                </a>
                                                </div>
                                            </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Tiger Nixon</td>
                                            <td>
                                            <div class="row">
                                                <div class="col-md-6 ">
                                                <a href="#" class="btn btn-warning btn-icon-split">
                                                    <span class="text">Edit</span>
                                                </a>
                                                </div>
                                                <div class="col-md-6">
                                                <a href="#" class="btn btn-danger btn-icon-split">
                                                <span class="text">Hapus</span>
                                                </a>
                                                </div>
                                            </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Tiger Nixon</td>
                                            <td>
                                            <div class="row">
                                                <div class="col-md-6 ">
                                                <a href="#" class="btn btn-warning btn-icon-split">
                                                    <span class="text">Edit</span>
                                                </a>
                                                </div>
                                                <div class="col-md-6">
                                                <a href="#" class="btn btn-danger btn-icon-split">
                                                <span class="text">Hapus</span>
                                                </a>
                                                </div>
                                            </div>
                                            </td>
                                        </tr>
                                       
                                    
                                    </tbody>
                                </table>
                                <div >
                                <a href="#" class="btn btn-primary btn-icon-split">
                                        <span class="text">Tambah</span>
                                    </a>
                                </div>
                                    
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
@endsection