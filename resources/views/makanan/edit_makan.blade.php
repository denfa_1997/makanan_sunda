@extends('layouts.master')


@section('content') 
<div id="content">
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Edit Tabel Makanan</h1>
                   

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Edit Data</h6>
                        </div>
                        <div class="card-body">
              <!-- /.card-header -->
              <!-- form start -->
              <form  role="form" action="/casts" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan nama">
                    @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="umur">Harga</label>
                    <input type="text" class="form-control" id="umur" name="umur" placeholder="Masukkan umur">
                    @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="biod">Stok</label>
                    <input type="text" class="form-control" id="umur" name="umur" placeholder="Masukkan umur">
                    @error('biod')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="biod">Gambar</label>
                    <input type="text" class="form-control" id="umur" name="umur" placeholder="Masukkan umur">
                    @error('biod')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
            </div>
            </div>

@endsection